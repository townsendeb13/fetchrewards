//
//  SeatGeekHelper.swift
//  FetchRewardsApp
//
//  Created by Eric Townsend on 6/25/21.
//

import Foundation

class SeatGeekHelper {
    
    static var shared = SeatGeekHelper()
    
    var clientId: String = "MTAwMDgxNjN8MTYyNDYyMDk3OC4wOTE1MDI3"
    var clientSecret: String = "8eb46413567cf11599e6a9344c7f0d9332683a8a3a9dc63a8d83bb65a47041f0"
    var seatGeekURL: String = "https://api.seatgeek.com/2"
    
    func getAllEvents(completionHandler: @escaping ([EventViewModel]) -> (Void)) {
        let url = URL(string: seatGeekURL + "/events?client_id=\(clientId)")
        let request = NSMutableURLRequest(url: url!)
        
        let session = URLSession(configuration: .default)
        session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            guard error == nil, let data = data else { return completionHandler([]) }
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] else { return completionHandler([]) }
            guard let events = jsonData["events"] as? [[String:Any]] else { return completionHandler([]) }
            
            var capturedEvents: [EventViewModel] = []
            
            for event in events {
                guard let eventJSON = try? JSONSerialization.data(withJSONObject: event, options: .prettyPrinted) else { continue }
                guard let eventModel = try? JSONDecoder().decode(Event.self, from: eventJSON) else { continue }
                capturedEvents.append(EventViewModel(event: eventModel))
            }
            
            completionHandler(capturedEvents)
        }.resume()
    }
    
    func getEventsWithSearch(term: String, completionHandler: @escaping ([EventViewModel]) -> (Void)) {
        let url = URL(string: seatGeekURL + "/events?q=\(term)&client_id=\(clientId)")
        let request = NSMutableURLRequest(url: url!)
        
        let session = URLSession(configuration: .default)
        session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            guard error == nil, let data = data else { return completionHandler([]) }
            
            guard let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] else { return completionHandler([]) }
            guard let events = jsonData["events"] as? [[String:Any]] else { return completionHandler([]) }
            
            var capturedEvents: [EventViewModel] = []
            
            for event in events {
                guard let eventJSON = try? JSONSerialization.data(withJSONObject: event, options: .prettyPrinted) else { continue }
                guard let eventModel = try? JSONDecoder().decode(Event.self, from: eventJSON) else { continue }
                capturedEvents.append(EventViewModel(event: eventModel))
            }
            
            completionHandler(capturedEvents)
        }.resume()
    }
}
