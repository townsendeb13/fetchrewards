//
//  EventViewModel.swift
//  FetchRewardsApp
//
//  Created by Eric Townsend on 6/25/21.
//

import Foundation
import UIKit
import CoreData

struct EventViewModel {
    
    var eventId: Int?
    var eventTitle: String?
    var eventLocation: String?
    var eventPhotoURL: String?
    var eventFormattedDate: String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        guard let eventDate = self.eventDate else { return nil }
        guard let date = formatter.date(from: eventDate) else { return nil }
        formatter.dateFormat = "EEEE, MMM d, yyyy h:mm a"
        
        return formatter.string(from: date)
    }
    
    private var eventDate: String?
    
    var isFavorited: Bool {
        guard let id = self.eventId else { return false }
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return false}
        
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSManagedObject>(entityName: "Favorite")
        
        guard let favorites = try? context.fetch(request) else { return false }
        guard let _ = favorites.first(where: { $0.value(forKeyPath: "eventId") as? String == String(describing: id) }) else { return false }
        
        return true
    }
    
    init(event: Event) {
        self.eventId = event.id
        self.eventTitle = event.title
        self.eventLocation = event.venueLocation
        self.eventDate = event.datetime_utc
        self.eventPhotoURL = event.performers.first?.image
    }
    
    func favoriteEvent() {
        guard let id = self.eventId else { return }
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let context = appDelegate.persistentContainer.viewContext
        
        if self.isFavorited {
            let request = NSFetchRequest<NSManagedObject>(entityName: "Favorite")
            guard let favorites = try? context.fetch(request) else { return }
            guard let favorite = favorites.first(where: { $0.value(forKeyPath: "eventId") as? String == String(describing: id) }) else { return }
            context.delete(favorite)
        } else {
            let entity = NSEntityDescription.entity(forEntityName: "Favorite", in: context)!
            let fav = NSManagedObject(entity: entity, insertInto: context)
            
            fav.setValue(String(describing: id), forKeyPath: "eventId")
              
            do {
                try context.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
}
