//
//  Event.swift
//  FetchRewardsApp
//
//  Created by Eric Townsend on 6/25/21.
//

import Foundation

struct Event: Codable {
    var id: Int?
    var title: String?
    var type: String?
    var datetime_utc: String?
    
    var venueName: String?
    var venueLocation: String?
    
    var performers: [Performer]
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case type = "type"
        case datetime_utc = "datetime_utc"
        case venue = "venue"
        case performers = "performers"
    }
    
    enum VenueKeys: CodingKey {
        case name, display_location
    }
    
    enum PerformerKeys: CodingKey {
        case name,  image
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.title = try container.decode(String.self, forKey: .title)
        self.type = try container.decode(String.self, forKey: .type)
        self.datetime_utc = try container.decode(String.self, forKey: .datetime_utc)
        self.performers = try container.decode([Performer].self, forKey: .performers)
        
        let venue = try container.nestedContainer(keyedBy: VenueKeys.self, forKey: .venue)
        self.venueName = try venue.decode(String.self, forKey: .name)
        self.venueLocation = try venue.decode(String.self, forKey: .display_location)
        
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.title, forKey: .title)
        try container.encode(self.type, forKey: .type)
        try container.encode(self.datetime_utc, forKey: .datetime_utc)
        try container.encode(self.performers, forKey: .performers)
        
        var venue = container.nestedContainer(keyedBy: VenueKeys.self, forKey: .venue)
        try venue.encode(self.venueName, forKey: .name)
        try venue.encode(self.venueLocation, forKey: .display_location)
    }
}
