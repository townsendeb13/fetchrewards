//
//  Performer.swift
//  FetchRewardsApp
//
//  Created by Eric Townsend on 6/25/21.
//

import Foundation

struct Performer: Codable {
    var name: String?
    var image: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case image = "image"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.image = try container.decode(String.self, forKey: .image)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.image, forKey: .image)
    }
}
