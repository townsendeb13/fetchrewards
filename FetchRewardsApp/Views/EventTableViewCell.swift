//
//  EventTableViewCell.swift
//  FetchRewardsApp
//
//  Created by Eric Townsend on 6/25/21.
//

import UIKit
import Kingfisher

protocol EventTableViewCellDelegate: AnyObject {
    func eventTableCellDidFavoriteEvent(cell: EventTableViewCell)
}

class EventTableViewCell: UITableViewCell {
    
    @IBOutlet private var eventPhoto: UIImageView!
    @IBOutlet private var eventTitle: UILabel!
    @IBOutlet private var eventLocation: UILabel!
    @IBOutlet private var eventDate: UILabel!
    @IBOutlet private var favoriteButton: UIButton!
    
    var model: EventViewModel?
    var delegate: EventTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellFrom(model: EventViewModel) {
        self.model = model
        self.configureFavoriteButton()
        
        self.eventTitle.text = model.eventTitle
        self.eventLocation.text = model.eventLocation
        self.eventDate.text = model.eventFormattedDate
        
        guard let photoURL = model.eventPhotoURL else { return }
        self.eventPhoto.kf.setImage(with: URL(string: photoURL))
    }
    
    func configureFavoriteButton() {
        guard let model = self.model else { return }
        
        if model.isFavorited {
            self.favoriteButton.setImage(UIImage(named: "filled-heart"), for: .normal)
        } else {
            self.favoriteButton.setImage(UIImage(named: "unfilled-heart"), for: .normal)
        }
    }
    
    @IBAction private func favoriteEvent(sender: UIButton) {
        self.delegate?.eventTableCellDidFavoriteEvent(cell: self)
    }
}
