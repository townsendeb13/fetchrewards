//
//  ViewController.swift
//  FetchRewardsApp
//
//  Created by Eric Townsend on 6/25/21.
//

import UIKit
import CoreData

class MainViewController: UIViewController {
    
    @IBOutlet private var searchBar: UISearchBar!
    @IBOutlet private var tableView: UITableView!
    
    var recentEvents: [EventViewModel]?
    var searchedEvents: [EventViewModel]?
    var favorites: [NSManagedObject] = []
    
    var inSearchMode: Bool {
        searchedEvents != nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.searchBar.delegate = self
        
        self.configureSearchBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.loadRecentEvents()
    }

    private func loadRecentEvents() {
        SeatGeekHelper.shared.getAllEvents { events in
            guard !events.isEmpty else { return }
            
            DispatchQueue.main.async {
                self.recentEvents = events
                self.tableView.reloadData()
            }
        }
    }
    
    private func configureSearchBar() {
        let textField = searchBar.value(forKey: "searchField") as! UITextField
        textField.textColor = .white

        let glassIconView = textField.leftView as! UIImageView
        glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
        glassIconView.tintColor = .white
    }
}

extension MainViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchedEvents = nil
            self.tableView.reloadData()
        } else {
            SeatGeekHelper.shared.getEventsWithSearch(term: searchText) { events in
                guard !events.isEmpty else { return }
                
                DispatchQueue.main.async {
                    self.searchedEvents = events
                    self.tableView.reloadData()
                }
            }
        }
    }
}

extension MainViewController: EventTableViewCellDelegate {
    func eventTableCellDidFavoriteEvent(cell: EventTableViewCell) {
        // Update the view model and then configure the button
        cell.model?.favoriteEvent()
        
        DispatchQueue.main.async {
            cell.configureFavoriteButton()
        }
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as? EventTableViewCell else { return EventTableViewCell() }
        guard let event = inSearchMode ? searchedEvents?[indexPath.row] : recentEvents?[indexPath.row] else { return EventTableViewCell() }
        cell.configureCellFrom(model: event)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let events = inSearchMode ? searchedEvents : recentEvents else { return 0 }
        return events.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 155.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        controller.event = inSearchMode ? searchedEvents?[indexPath.row] : recentEvents?[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

