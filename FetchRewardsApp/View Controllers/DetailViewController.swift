//
//  DetailViewController.swift
//  FetchRewardsApp
//
//  Created by Eric Townsend on 6/25/21.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet private var eventPhoto: UIImageView!
    @IBOutlet private var eventTitle: UILabel!
    @IBOutlet private var eventLocation: UILabel!
    @IBOutlet private var eventDate: UILabel!
    @IBOutlet private var favoriteButton: UIButton!
    
    var event: EventViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let event = self.event else { return }
        
        self.eventTitle.text = event.eventTitle
        self.eventLocation.text = event.eventLocation
        self.eventDate.text = event.eventFormattedDate
        
        self.configureFavoriteButton()
        
        guard let photoURL = event.eventPhotoURL else { return }
        self.eventPhoto.kf.setImage(with: URL(string: photoURL))
    }
    
    func configureFavoriteButton() {
        guard let model = self.event else { return }
        
        if model.isFavorited {
            self.favoriteButton.setImage(UIImage(named: "filled-heart"), for: .normal)
        } else {
            self.favoriteButton.setImage(UIImage(named: "unfilled-heart"), for: .normal)
        }
    }
    
    @IBAction private func favoriteEvent(sender: UIButton) {
        // Update the view model and then configure the button
        self.event?.favoriteEvent()
        
        DispatchQueue.main.async {
            self.configureFavoriteButton()
        }
    }
    
    @IBAction private func goBack(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
