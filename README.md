# README #

### What is this repository for? ###

This is a code test for FetchRewards in which we consume the SeatGeek API and use it to display and search events in their database.

Features
- Discover recent events
- Live Search SeatGeeks Events API for the top 10 results
- Display a graphical representation of the event on a detail screen

![Alt text](https://i.ibb.co/9Tx3HsS/Simulator-Screen-Shot-i-Phone-12-2021-06-25-at-11-44-23.png)
![Alt text](https://i.ibb.co/BTLW3r5/Simulator-Screen-Shot-i-Phone-12-2021-06-25-at-11-44-27.png)
![Alt text](https://i.ibb.co/NVc96nM/Simulator-Screen-Shot-i-Phone-12-2021-06-25-at-11-44-34.png)

Code Exercise - https://fetch-hiring.s3.amazonaws.com/iOS+coding+exercise.pdf

### How do I get set up? ###

No set up needed! Just clone the project and run the .xcworkspace to use the mobile app.

